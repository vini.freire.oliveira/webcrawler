- ## ZAG WEB CRAWLER & Logic 

  #####  About

  A **Web crawler**, sometimes called a **spider** or **spiderbot** and often shortened to **crawler**, is an [Internet bot](https://en.wikipedia.org/wiki/Internet_bot) that systematically browses the [World Wide Web](https://en.wikipedia.org/wiki/World_Wide_Web), typically for the purpose of [Web indexing](https://en.wikipedia.org/wiki/Web_indexing) (*web spidering*).

  

  #####  Ops

  - This crawler was developed using Docker.

    

  #####  System dependencies

  - Docker (<https://www.docker.com/>)
  - Docker-compose

  

  #####  Technologies

  - Ruby 2.5

  - Rails 5.2

  - Postgres

  - Redis

    

  #####  Gems

  - gem 'sidekiq' Simple, efficient background processing for Ruby.

  - gem 'nokogiri' simple, is an HTML, XML, SAX, and Reader parser.

  - gem 'rest-client' A simple HTTP and REST client for Ruby.

  - gem 'brakeman' brakeman is a security scanner for Ruby on Rails applications.

    - The brakeman results is in **docs path as [security-report.html]()** 

  - The following gems were used to help in the automated tests

    - rspec
    - capybara
    - ffaker
    - factory_bot_rails
    - The Rspec results is in **docs spec-results.html**

    

  #####  Step by step to run the project in localhost using docker-compose

  1. Clone the project 

     ```bash
     git clone https://gitlab.com/vini.freire.oliveira/webcrawler.git
     ```

  2. Build the containers and install all dependencies

     ```
     docker-compose run --rm app bundle install
     ```

  3. Now, create database and generate migrations:

     ```
     docker-compose run --rm app bundle exec rails db:create db:migrate
     ```

  4. Start the server with

     ```
     docker-compose up
     ```



**Logic Test**

1. **The robot_zag.rb is in docs folder.**
   * Create a virtual robot, that can walk up, right, left and bottom.



###  Links

#####  	[My Git](https://gitlab.com/vini.freire.oliveira) 

#####  	[My LikedIn](https://www.linkedin.com/in/vinicius-freire-b53507107/)