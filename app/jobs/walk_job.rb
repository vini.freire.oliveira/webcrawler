class WalkJob < ApplicationJob
  queue_as :sites

  def perform(url)
    site = Site.create(url: url)
    tags = CrawlerService.new(url)
    data = tags.crawler_data
    data[:site_id] = site.id
    
    tag = Tag.new(data)
    tag.save
  end
end
