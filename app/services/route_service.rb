class RouteService
  attr_accessor :url

  def initialize (url)
    @url = url
  end

  def walk
    sites = []
    site = Site.where(url: @url).first
    links = site.tag.first.link.split(",")
    links.each do |link|
      next if link.match(/https:|http:|irc:/)
      sites << link.chars[2..link.length-2].join if link.length > 4
    end
    sites
  end
end
