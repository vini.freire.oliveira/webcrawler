FactoryBot.define do
  factory :site do
      url { FFaker::Internet.unique.http_url }
  end
end
