require "rails_helper"

RSpec.feature "User click on Elixir Crawler" do

    scenario "user visit the root_path and check all sites" do
        10.times do
            FactoryBot.create(:site)
        end

        visit  root_path

        Site.all.each do |site|
            expect(page).to have_content("Url Action")
            expect(page).to have_content(site.url)
            expect(page).to have_content("Show Content Url Show Edit Delete")
        end
    end

    scenario "user visit the root_path and click on Elixir Crawler" do
      visit  root_path
      click_on "Elixir Crawler"

      Site.all.each do |site|
        expect(page).to have_content("Url Action")
        expect(page).to have_content(site.url)
        expect(page).to have_content("Show Content Url Show Edit Delete")
      end

    end
end
