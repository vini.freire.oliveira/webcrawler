require 'rails_helper'

RSpec.describe Site, type: :model do
  
  before(:each) do
    @site = FactoryBot.create(:site)
  end

  describe 'Associations' do
    it { should have_many :tag }
  end

  describe 'Validations' do
    context 'url' do
        it 'should be unique' do
            site_repeated = FactoryBot.create(:site)
            site_repeated.url = @site.url
            expect(site_repeated).to_not be_valid
        end

        it 'should not be blank' do
            @site.url = nil
            expect(@site).to_not be_valid
        end
    end
  end 

end
