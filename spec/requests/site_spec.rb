require 'rails_helper'
 
RSpec.describe "Site", type: :request do
  
  describe "POST /create" do
    
    before do
      post "/sites", params: { site: {url: "https://elixir-lang.org/"} }
    end

    it "returns found" do
      expect(response.status).to eql(302)
    end

    context "analyzing website home" do
      before do
        @site = Site.where(url: "https://elixir-lang.org/").first
      end

      it "Save the site of elixir" do
        expect(@site.url).to eql("https://elixir-lang.org/")
      end

      it "Save the content of elixir home" do
        expect(@site.tag).to_not be_nil
        tag = @site.tag.first
        expect(tag.h1).to_not be_nil
        expect(tag.h2).to_not be_nil
        expect(tag.h3).to_not be_nil
        expect(tag.h4).to_not be_nil
        expect(tag.h5).to_not be_nil
        expect(tag.h6).to_not be_nil
        expect(tag.link).to_not be_nil
        expect(tag.image).to_not be_nil
      end

      context "analyzing the crawling founded from elixir home page" do
        it "save all link that belongs to elixir website" do
          base_url = "https://elixir-lang.org/"
          
          ["/development.html","/install.html","/learning.html","/blog/","/docs.html","/getting-started/introduction.html",
          "/getting-started/introduction.html","/getting-started/introduction.html","/crash-course.html",
          "/blog/2019/06/24/elixir-v1-9-0-released/"].each do |link|
            site = Site.where(url: base_url+link).first
            expect(site.url).to eql(base_url+link)
            expect(site.tag).to_not be_nil
            tag = site.tag.first
            expect(tag.h1).to_not be_nil
            expect(tag.h2).to_not be_nil
            expect(tag.h3).to_not be_nil
            expect(tag.h4).to_not be_nil
            expect(tag.h5).to_not be_nil
            expect(tag.h6).to_not be_nil
            expect(tag.link).to_not be_nil
            expect(tag.image).to_not be_nil
          end
        end
        
      end

    end

  end
end