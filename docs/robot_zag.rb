class Edge
  attr_accessor :src, :dst, :length
  
  def initialize(src, dst, length = 1)
    @src = src
    @dst = dst
    @length = length
  end
end

class Graph < Array
  attr_reader :edges
  
  def initialize
    @edges = []
  end
  
  def connect(src, dst, length = 1)
    unless self.include?(src)
      raise ArgumentException, "No such vertex: #{src}"
    end
    unless self.include?(dst)
      raise ArgumentException, "No such vertex: #{dst}"
    end
    @edges.push Edge.new(src, dst, length)
  end
  
  def connect_mutually(vertex1, vertex2, length = 1)
    self.connect vertex1, vertex2, length
    self.connect vertex2, vertex1, length
  end

  def neighbors(vertex)
    neighbors = []
    @edges.each do |edge|
      neighbors.push edge.dst if edge.src == vertex
    end
    return neighbors.uniq
  end

  def length_between(src, dst)
    @edges.each do |edge|
      return edge.length if edge.src == src and edge.dst == dst
    end
    nil
  end

  def dijkstra(src, dst = nil)
    distances = {}
    previouses = {}
    self.each do |vertex|
      distances[vertex] = nil # Infinity
      previouses[vertex] = nil
    end
    distances[src] = 0
    vertices = self.clone
    until vertices.empty?
      nearest_vertex = vertices.inject do |a, b|
        next b unless distances[a]
        next a unless distances[b]
        next a if distances[a] < distances[b]
        b
      end
      break unless distances[nearest_vertex] # Infinity
      if dst and nearest_vertex == dst
        return distances[dst]
      end
      neighbors = vertices.neighbors(nearest_vertex)
      neighbors.each do |vertex|
        alt = distances[nearest_vertex] + vertices.length_between(nearest_vertex, vertex)
        if distances[vertex].nil? or alt < distances[vertex]
          distances[vertex] = alt
          previouses[vertices] = nearest_vertex
        end
      end
      vertices.delete nearest_vertex
    end
    if dst
      return nil
    else
      return distances
    end
  end
end

module Connection
  def make_connection row,col
    connections = []
    connections << move_right(row,col)
    connections << move_left(row,col)
    connections << move_down(row,col)
    connections.compact
  end

  private 

  def move_up row,col
    return "#{row-1},#{col}" if row-1 > -1 && @matrix[row-1][col] != 0
  end

  def move_down row,col
    return "#{row+1},#{col}" if row+1 < @matrix.count && @matrix[row+1][col] != 0
  end

  def move_left row,col
    return "#{row},#{col-1}" if col-1 > -1 && @matrix[row][col-1] != 0
  end

  def move_right row,col
    return "#{row},#{col+1}" if col+1 < @matrix[0].count && @matrix[row][col+1] != 0
  end
end

class Robot
  include Connection
  
  attr_reader :graph, :src, :dst

  def initialize matrix
    @matrix = matrix
    @graph = Graph.new
    @src = @dst = "0,0"
    create_graph
    connect_mutually
  end

  def best_route
    @graph.dijkstra @src, @dst
  end

  def route_between src,dst
    @graph.length_between src, dst
  end

  def neighbors src
    @graph.neighbors src
  end

  private

  def create_graph
    @matrix.each_with_index do |e, row|
      e.each_with_index do |value,col|
        @graph.push "#{row},#{col}" if value != 0
        @dst = "#{row},#{col}" if value == 9
      end
    end
  end

  def connect_mutually
    @graph.each do |pos|
      coordinates = pos.split(",")
      connections = make_connection(coordinates[0].to_i,coordinates[1].to_i)
      connections.each {|connection| @graph.connect_mutually pos, connection}
    end
    
  end
end


matrix = [[1,1,1,1],
          [0,1,1,0],
          [0,1,0,1],
          [0,1,9,1],
          [1,1,1,1]]

robot = Robot.new matrix

p robot.graph
# p robot.route_between("0,0", "0,1")
# p robot.neighbors("0,0")
p robot.best_route








