class CreateTags < ActiveRecord::Migration[5.2]
  def change
    create_table :tags do |t|
      t.text :h1
      t.text :h2
      t.text :h3
      t.text :h4
      t.text :h5
      t.text :h6
      t.text :link
      t.text :image
      t.references :site, foreign_key: true

      t.timestamps
    end
  end
end
