require 'sidekiq/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'
  resources :sites
  resources :tags, only: [:show,:create]

  root to: 'sites#index'
end
